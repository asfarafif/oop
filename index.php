
<?php

// require('animal.php');
require('frog&ape.php');

$sheep = new Animal("shaun");
    echo "nama: $sheep->name <br>"; // "shaun"
    echo "legs: $sheep->legs <br>"; // 4
    echo "cold_blooded: $sheep->cold_blooded <br><br>"; // "no"
    
$kodok = new Frog("buduk");
    echo "nama: $kodok->name <br>"; 
    echo "legs: $kodok->legs <br>"; 
    echo "cold_blooded: $kodok->cold_blooded <br>"; 
    $kodok->jump() ; // "hop hop";
    echo "<br><br>";

$sungokong = new Ape("kera sakti");
    echo "nama: $sungokong->name <br>"; 
    echo "legs: $sungokong->legs <br>"; 
    echo "cold_blooded: $sungokong->cold_blooded <br>"; 
    $sungokong->yell(); // "Auooo";
    echo "<br><br>";
 
 ?>

 

 